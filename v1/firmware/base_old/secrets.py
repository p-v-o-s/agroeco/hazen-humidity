# SPDX-FileCopyrightText: 2020 Adafruit Industries
#
# SPDX-License-Identifier: Unlicense

# This file is where you keep secret settings, passwords, and tokens!
# If you put them in the code you risk committing that info or sharing it

secrets = {
    'ssid' : 'Fios-7S5yS_EXT',
    'password' : 'whose44gel62has',
    'bayou_public' : 'z83n4f9kwkmc',
    'bayou_private' : 'wku6cx3pyz9g',
    'timezone' : "America/New_York", # http://worldtimeapi.org/timezones
    }
